# 16 is the current LTS version
# Use the Apline image for smaller image size
FROM node:16-alpine

WORKDIR /opt/app

# This layer only rebuilt when package.json is updated 
COPY ./node_modules ./node_modules

# This layer is rebuilt when changes in src folder found
COPY ./dist ./dist

CMD ["node", "dist/main.js"]