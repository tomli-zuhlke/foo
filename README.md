

## SonarCloud badges

[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=coverage)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=bugs)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=tomli-zuhlke_foo&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=tomli-zuhlke_foo)


## To build docker image and run it locally
- Run
  ```
  docker build --tag foo:0.1.0 .
  ```

- Run
  ```
  docker run -p 3000:3000 foo:0.1.0
  ```

- Open a browser and navigate to http://localhost:3000
